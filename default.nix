{ pkgs ? import <nixpkgs> {} }:

with pkgs;

stdenv.mkDerivation rec {
    name = "hello";
    version = "2.12";
    src = fetchurl {
        url = "https://mirrors.sarata.com/gnu/hello/hello-${version}.tar.gz";
        hash = {
            "2.12" = "sha256-zwSvhtwIUmjF9EcPuuSbGK+8Iht4CWqrhC2TSna60Ks=";
            "2.11" = "sha256-jJzgVy08RO0GcOsc3pgFhOA4tvYsJf396O8SjeFQBL0=";
            "2.10" = "sha256-MeBmE3qWJnbon2nRtlOC3pWn732RS4y5VvQepy4PUWs=";
            "2.9" = "sha256-7Lt6IhQZbFf/k0CqcUWOFVmr049tjRaWZoRpNd8ZHqc=";
            "2.8" = "sha256-5rd/gffPfa761Kn1tl3myunD8TuM+66oy1O7XqVGDXM=";
            "2.2" = "sha256-aaKGGx/OxlfBoOyKh704+48AcqHewIX91WU9qsm+V1E=";
        }.${version} or "sha256-0000000000000000000000000000000000000000000=";
    };
#buildPhase = "gcc -o hello ./hello.c";
#installPhase = "mkdir -p $out/bin && install -t $out/bin hello";

    meta = {
        description = "GNU Hello";
        homepage = "https://www.gnu.org/software/hello/";
        license = "GPLv3+";
    };
}

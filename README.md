#  nix-flake-hello

A nix flake for GNU Hello, because I need something to practice with.


## Use

On a flake-enabled nix system (see https://nixos.wiki/wiki/Flakes), either:

Clone this repo and
```
nix run
```
or
```
nix build
./result/bin/hello
```

Or just run directly with
```
nix run gitlab:yjftsjthsd-g/nix-flake-hello
```

You can also run a specific version by its git tag:
```
nix run gitlab:yjftsjthsd-g/nix-flake-hello/v2.10 -- --version
```
Although of course this only works for versions that I've actually tagged
(https://gitlab.com/yjftsjthsd-g/nix-flake-hello/-/tags)


## License

The contents of this repo is under the MIT license (see LICENSE file), but note
that of course this only covers the contents of *this repo*, which is config
files to build GNU Hello, which is under the GPL.

